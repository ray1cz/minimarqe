/*
Copyright (C) 2021 Rene Lauer, ray@phalanx.cz

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 MA 02110-1301  USA
*/
package cz.phalanx.mdn;

import cz.phalanx.mdn.MarkdownToken.Type;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *	MarkdownTranslator uses a supplied MarkdownLexer to lexically analyze the supplied text obtaining a stream of tokens.
 *	These tokens are subsequently parsed (grouped) and translated to HTML tags. The replacement is textual: the translator
 *	generates proper HTML source, but all text (except white-space) is put out verbatim, including injected HTML tags, which can
 *	lead to an invalid HTML fragment source. 
 *	<p>
 *	Applications using this library are required to handle the injected tags appropriately <b>before</b> passing the source to the
 *	translator. A particular way of handling is application-dependent; the application may, for example, decide to:
 *	<ul>
 *		<li>escape the tags so they appear as text</li>
 *		<li>filter out the tags</li>
 *		<li>reject the source text altogether</li>
 *	</ul>
 * @author Rene Lauer (ray@phalanx.cz)
 */
public class MarkdownTranslator
{
	private static final Logger LOG = Logger.getLogger(MarkdownTranslator.class.getName());

	private final MarkdownLexer lexer;

	/**
	 *	Create a translator that uses the supplied lexer to extract tokens from the text.
	 * @param lexer	the lexical analyzer
	 */
	public MarkdownTranslator(MarkdownLexer lexer)
	{
		this.lexer = lexer;
	}
	
	/**
	 *	Translate the input sequence with markdown to HTML with corresponding mark-up. The translator recognizes the following rules:
	 *	<ul>
	 *		<li><b>*</b>text<b>*</b> is translated to HTML bold text in &lt;b&gt; tags</li>
	 *		<li><b>/</b>text<b>/</b> is translated to HTML italic text in &lt;i&gt; tags</li>
	 *		<li><b>_</b>text<b>_</b> is translated to HTML underlined text in &lt;u&gt; tags</li>
	 *		<li><b>[</b>URL link text<b>]</b> is translated to HTML anchor &lt;a href="${URL}"&gt;${link text}&lt;a&gt;</li>
	 *		<li><b>{</b>URL<b>}</b> is translated to HTML image &lt;img src="${URL}"&gt;</li>
	 *		<li><b>#</b>text is translated to a list item in unordered list, which must be terminated by an empty line</li>
	 *		<li><b>empty line</b> not terminating the unordered list is translated to a line break &lt;br&gt;</li>
	 *	</ul>
	 *	<p>
	 *	Wherever a text is allowed in the rule, another markdown can be nested in, thus for example *_bold underline_* translates to &lt;b&gt;&lt;u&gt;bold underline&lt;u&gt;&lt;b&gt;. The
	 *	markdown must be nested properly, otherwise the translator throws an exception. All LWSP text is reduced to a single space.
	 *	<p>
	 *	When a markdown mark (i.e. *, /, _, [, ], {, } or #) is doubled (e.g. **), it stands for the character itself and loses its special meaning. This feature slightly complicates writing
	 *	the absolute URLs in link and image: while all characters in URL lose their special meaning and do not require doubling, doubled characters are recognized directly by the lexer and
	 *	reduced to singles. That causes the necessity to double at least one of the doubled characters in an URL, e.g. https://www.google com must be written as http:<b>///</b>www.google.com.
	 *	LWSP ends URL token and as such must not be present within the URL (which is demanded by the URL itself).

	 * @param input	the marked-down text
	 * @return	the HTML with mark-up
	 * @throws MarkdownException when there is a syntax error in the markdown
	 */
	public String translate(CharSequence input) throws MarkdownException
	{
		Stream<MarkdownToken> tokens = this.lexer.tokenize(input);
		return parseAndGenerate(tokens);
	}
	
	private boolean isOnTop(Stack<MarkdownToken> stack, MarkdownToken token)
	{
		if (stack.isEmpty())
			return false;
		else
			return stack.peek().type() == token.type();
	}
	
	private boolean isOnTop(Stack<MarkdownToken> stack, MarkdownToken.Type type)
	{
		if (stack.isEmpty())
			return false;
		else
			return stack.peek().type() == type;
	}
	
	protected String parseAndGenerate(final Stream<MarkdownToken> tokens) throws MarkdownException
	{
		try
		{
			AtomicInteger linkPhase = new AtomicInteger(0);
			AtomicInteger imgPhase = new AtomicInteger(0);
			final Stack<MarkdownToken> stack = new Stack<>();
			String result = 
				tokens
						.map(tkn -> {
							String retval ="";
							final MarkdownToken token;
							if (
									(	
										(isOnTop(stack, Type.LINK_OPEN) && linkPhase.get() < 2) ||
										(isOnTop(stack, Type.IMAGE_OPEN) && tkn.type() != Type.IMAGE_CLOSE)
									)
									&& (tkn.type().compareTo(Type.TEXT) < 0)
							)
								token = new ReplacementText(tkn);
							else
								token = tkn;
							switch (token.type())
							{
								case ERROR:	
											throw new MarkdownError(token);
								case BOLD:	
								case ITALIC:
								case UNDERLINE:
											if (isOnTop(stack, token))
											{
												retval = token.closeTag();
												stack.pop();
											}
											else
											{
												retval = token.openTag();
												stack.push(token);
											}
											break;
								case LINK_OPEN:
											stack.push(token);
											linkPhase.set(0);
											break;
								case LINK_CLOSE:
											if (isOnTop(stack, Type.LINK_OPEN))
											{
												stack.pop();
												if (linkPhase.get()==0)
													retval = "[]";
												else if (linkPhase.get() > 1)
													retval = token.closeTag();
												else
													throw new MarkdownError(token, "Incomplete hyperlink");
											}
											else
												retval = "]";		// ignore unmatched closes
											break;
								case BULLET:
											if (isOnTop(stack, token))
												retval = token.closeTag();
											else
											{
												stack.push(token);
												retval = token.openTag();
											}
											break;
								case IMAGE_OPEN:
											stack.push(token);
											imgPhase.set(0);
											break;
								case IMAGE_CLOSE:
											if (isOnTop(stack, Type.IMAGE_OPEN))
											{
												stack.pop();
												if (imgPhase.get() == 0)
													retval = "{}";
												else 
													retval = token.closeTag();
											}
											else
												retval = "}";
											break;
								case BLANK:
											if (isOnTop(stack, Type.BULLET))
											{
												stack.pop();
												retval = token.closeTag();
											}
											else
												retval = token.openTag();
											break;
								case LWSP:
											if (isOnTop(stack, Type.LINK_OPEN))
											{
												switch (linkPhase.get())
												{
													case 0:	// the first word was not yet seen
															retval = "";
															break;
													case 1:	// this is a space between url and text
															retval = stack.peek().closeTag();
															linkPhase.incrementAndGet();
															break;
													default:
															retval = " ";
															break;
												}
											}
											else if (isOnTop(stack, Type.IMAGE_OPEN))
												retval = "";
											else
												retval = token.text();
											break;
								case TEXT:
											if (isOnTop(stack, Type.LINK_OPEN))
											{
												switch (linkPhase.get())
												{
													case 0:	retval = stack.peek().openTag() + token.text();
															linkPhase.incrementAndGet();
															break;
													case 1:	retval = token.text();
															break;
													default:
															retval = token.text();
															break;
												}
											}
											else if (isOnTop(stack, Type.IMAGE_OPEN))
											{
												switch (imgPhase.get())
												{
													case 0:	retval = stack.peek().openTag()+ token.text();
															imgPhase.incrementAndGet();
															break;
													default:
															retval = token.text();
															break;
												}
											}
											else
												retval = token.text();
											break;
							}
							return retval;
						})
						.collect(Collectors.joining());
			if (stack.isEmpty())
				return result;
			else
			{
				throw new MarkdownException(stack.pop(), "Unclosed mark");
			}
		}
		catch (MarkdownError er)
		{
			throw new MarkdownException(er.getToken(), er.getMessage());
		}
	}
		
	private class MarkdownError extends Error
	{
		private final MarkdownToken token;

		public MarkdownError(MarkdownToken token)
		{
			this.token = token;
		}

		public MarkdownError(MarkdownToken token, String string)
		{
			super(string);
			this.token = token;
		}

		public MarkdownError(MarkdownToken token, String string, Throwable thrwbl)
		{
			super(string, thrwbl);
			this.token = token;
		}

		public MarkdownError(MarkdownToken token, Throwable thrwbl)
		{
			super(thrwbl);
			this.token = token;
		}

		public MarkdownError(MarkdownToken token, String string, Throwable thrwbl, boolean bln, boolean bln1)
		{
			super(string, thrwbl, bln, bln1);
			this.token = token;
		}

		public MarkdownToken getToken()
		{
			return token;
		}
		
	}
	
	private class ReplacementText implements MarkdownToken
	{
		
		private final MarkdownToken wrapped;

		public ReplacementText(MarkdownToken wrapped)
		{
			this.wrapped = wrapped;
		}
		
		@Override
		public Type type()
		{
			return Type.TEXT;
		}

		@Override
		public String text()
		{
			return wrapped.text();
		}

		@Override
		public String openTag()
		{
			return wrapped.openTag();
		}

		@Override
		public String closeTag()
		{
			return wrapped.closeTag();
		}

		@Override
		public int line()
		{
			return wrapped.line();
		}

		@Override
		public int pos()
		{
			return wrapped.pos();
		}
	}
}
