/*
Copyright (C) 2021 Rene Lauer, ray@phalanx.cz

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 MA 02110-1301  USA
*/
package cz.phalanx.mdn;

/**
 *	This interface must be implemented by all objects that are returned as part of the token stream from MarkdownLexer.
 * @author Rene Lauer (ray@phalanx.cz)
 */
public interface MarkdownToken
{
	public enum Type
	{
		/** The bold mark (asterisk).*/
		BOLD,
		/** The italic mark (forward slash).*/
		ITALIC,
		/** The underline mark (underscore).*/
		UNDERLINE,
		/** Opening of a hyperlink (left square bracket).*/
		LINK_OPEN,
		/** Closing of a hyperlink (right square bracket).*/
		LINK_CLOSE,
		/** Unordered list item bullet (hashmark). */
		BULLET,
		/** Opening of an image (less-than). */
		IMAGE_OPEN,
		/** Closing of an image (greater-than).*/
		IMAGE_CLOSE,
		/** Blank line (two enters).*/
		BLANK,
		/** Text token. */
		TEXT,
		/** Linear whitespace. */
		LWSP,
		/** Error token. */
		ERROR;
	}
	
	/**
	 *	Return token type as defined by the enclosed enum (lexical value).
	 * @return a token type
	 */
	public Type type();
	
	/**
	 *	Return the text carried with the token (semantic value), if any.
	 * @return the text of the token
	 */
	public String text();
	
	/**
	 *	Return the opening tag for HTML output.
	 * @return a text representing the opening tag in HTML
	 */
	public String openTag();
	
	/**
	 *	Return the closing tag for HTML output.
	 * @return closing tag text
	 */
	public String closeTag();
	
	/**
	 *	Line position of the token.
	 * @return line number starting at 1
	 */
	public int line();
	
	/**	
	 *	Character position relative to the line start.
	 * @return character pos starting at 1
	 */
	public int pos();
}
