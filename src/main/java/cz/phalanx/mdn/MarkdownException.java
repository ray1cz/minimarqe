/*
Copyright (C) 2021 Rene Lauer, ray@phalanx.cz

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 MA 02110-1301  USA
*/
package cz.phalanx.mdn;

/**
 *	Error reporting for the markdown translation.
 *	This exception class contains reference to a token that caused the error, thus being able to report the spot of occurrence.
 * @author Rene Lauer (ray@phalanx.cz)
 */
public class MarkdownException extends Exception
{
	private final MarkdownToken token;

	/**
	 *	Constructs an exception with the token. The message is set to "Error near" for all other token types
	 *	except ERROR, which carries its own message.
	 * @param token	the token causing the error
	 */
	public MarkdownException(MarkdownToken token)
	{
		super("Error near ");
		this.token = token;
	}

	/**
	 *	Constructs an exception with the token and the message.Tokens of Type.ERROR ignore the message.
	 * @param token	the token causing the error
	 * @param string	message to report
	 */
	public MarkdownException(MarkdownToken token, String string)
	{
		super(string);
		this.token = token;
	}

	/**
	 *	Constructs an exception with the token and the message.Tokens of Type.ERROR ignore the message.Allows specifying an underlying cause exception.
	 * @param token	the token causing the error
	 * @param string	the message
	 * @param thrwbl	the cause
	 */
	public MarkdownException(MarkdownToken token, String string, Throwable thrwbl)
	{
		super(string, thrwbl);
		this.token = token;
	}

	/**
	 *	Constructs an exception with the token and the cause.The message is set to "Error near" for all token types except ERROR, which carries its own message.
	 * @param token	the token causing the error
	 * @param thrwbl	the cause
	 */
	public MarkdownException(MarkdownToken token, Throwable thrwbl)
	{
		super("Error near ", thrwbl);
		this.token = token;
	}


	@Override
	public String getLocalizedMessage()
	{
		if (this.token.type() == MarkdownToken.Type.ERROR)
			return this.token.text();
		else
			return super.getMessage() + " " + this.token.type().name() + " at line "+this.token.line()+", pos " + this.token.pos();
	}

	@Override
	public String getMessage()
	{
		return this.getLocalizedMessage();
	}
}
