/*
Copyright (C) 2021 Rene Lauer, ray@phalanx.cz

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 MA 02110-1301  USA
*/
package cz.phalanx.mdn;

import java.util.Spliterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 *	This class' instance breaks the provided text to tokens for parsing. It recognizes the following patterns:
 *	<ul>
 *		<li><b>*</b> not preceded by another * = bold</li>
 *		<li><b>/</b> not preceded by another / = italics</li>
		<li><b>_</b> not preceded by another _ = underline</li>
 *		<li><b>[</b> url text <b>]</b> = hyperlink with description</li>
 *		<li><b>{</b>url<b>}</b> = image from the URL</li>
 *		<li><b>#</b> not preceded by another # =  unordered list item, must be terminated by an empty line</li>
 *		<li>empty line = end of unordered list, if open, otherwise a line break (&lt;br&gt;)</li>
 *	</ul>
 *	
 * @author Rene Lauer (ray@phalanx.cz)
 */
public class MarkdownLexer
{

	private static final Logger LOG = Logger.getLogger(MarkdownLexer.class.getName());

	private static final Pattern PBOLD = Pattern.compile("\\*{1,2}");
	private static final Pattern PITALIC = Pattern.compile("/{1,2}");
	private static final Pattern PUNDER = Pattern.compile("_{1,2}");
	private static final Pattern PLOPEN = Pattern.compile("\\[{1,2}");
	private static final Pattern PLCLOSE = Pattern.compile("\\]{1,2}");
	private static final Pattern PBULLET = Pattern.compile("#{1,2}");
	private static final Pattern PBLANK = Pattern.compile("\n[ \t]*\n");
	private static final Pattern PLWSP = Pattern.compile("[ \t]+");
	private static final Pattern PTEXT = Pattern.compile("\n|[^*/_#\\[\\]\\{\\}\n \t]+");
	private static final Pattern PIMOPEN = Pattern.compile("\\{{1,2}");
	private static final Pattern PIMCLOSE = Pattern.compile("\\}{1,2}");
	
	private Spliterator<MarkdownToken> spilterate(CharSequence source)
	{
		AtomicInteger line = new AtomicInteger(1);
		AtomicInteger pos = new AtomicInteger(1);
		final StringBuilder  src = new StringBuilder(source);
		
		Pattern[] patterns = new Pattern[] {
			PBOLD, PITALIC, PUNDER, PLOPEN, PLCLOSE, PBULLET, PIMOPEN, PIMCLOSE, PBLANK, PLWSP, PTEXT
		};
		
		return new Spliterator<MarkdownToken>() 
		{
			@Override
			public boolean tryAdvance(Consumer<? super MarkdownToken> action)
			{
				if (src.length() < 1)
					return false;
				
				Matcher m = null;
				int i = 0;
				while (i < patterns.length)
				{
					m = patterns[i].matcher(src);
					if (m.lookingAt())
						break;
					else
						i++;
				}
				if (i >= patterns.length)
				{
					action.accept(new ErrorToken(src.toString(), line.get(), pos.get()));
					return false;
				}
				else
				{
					MarkdownToken token;
					if (i < 8 && m.end() > 1)	// this is a doubled mark
						token = new TextToken(line.get(), pos.get(), m.group().substring(0, 1));
					else
					{
						switch (i)
						{
							case 0:	token = new BoldToken(line.get(), pos.get()); break;
							case 1:	token = new ItalicToken(line.get(), pos.get()); break;
							case 2:	token = new UnderlineToken(line.get(), pos.get()); break;
							case 3:	token = new LinkOpenToken(line.get(), pos.get()); break;
							case 4:	token = new LinkCloseToken(line.get(), pos.get()); break;
							case 5:	token = new BulletToken(line.get(), pos.get()); break;
							case 6:	token = new ImageOpenToken(line.get(), pos.get()); break;
							case 7:	token = new ImageCloseToken(line.get(), pos.get()); break;
							case 8:	token = new BlankToken(line.get(), pos.get()); break;
							case 9:	token = new LwspToken(line.get(), pos.get(), m.group()); break;
							case 10:	token = new TextToken(line.get(), pos.get(), m.group()); break;
							default:
									throw new IllegalStateException("Token table inconsistent");
						}
					}
					action.accept(token);
					String tkn = m.group();
					int ndx = tkn.lastIndexOf('\n');
					if (ndx < 0)
						pos.addAndGet(m.end());
					else
					{
						int p = 0;
						
						while ((p = tkn.indexOf('\n', p)) > 0)
						{
							p++;
							line.incrementAndGet();
						}
						pos.set(tkn.length() - ndx);
					}
					src.delete(0, m.end());
					return true;
				}
			}

			@Override
			public Spliterator<MarkdownToken> trySplit()
			{
				return null;
			}

			@Override
			public long estimateSize()
			{
				return Long.MAX_VALUE;
			}

			@Override
			public int characteristics()
			{
				return Spliterator.IMMUTABLE | Spliterator.NONNULL | Spliterator.ORDERED;
			}
		};
	}
	
	/**
	 *	The method breaks the input character sequence into tokens according to rules/patterns and provides a stream of those tokens.
	 *	
	 * @param sequence the text to be lexically analyzed 
	 * @return	a stream of tokens. There can be a premature end of the tokenization indicated by the presence of token with Type.ERROR at the end of the stream.
	 */
	public Stream<MarkdownToken> tokenize(CharSequence sequence)
	{
		Spliterator<MarkdownToken> spilterator = this.spilterate(sequence);
		return StreamSupport.stream(spilterator, false);
	}
	
	private abstract class PositionToken implements MarkdownToken
	{
		private final int line;
		private final int pos;
		
		protected PositionToken(int line, int pos)
		{
			this.line = line;
			this.pos = pos;
		}

		@Override
		public int line()
		{
			return this.line;
		}

		@Override
		public int pos()
		{
			return this.pos;
		}
	}
	
	private abstract class EmptyToken extends PositionToken
	{
		private Type type;
		private final String tagBase;
		
		protected EmptyToken(int line, int pos, Type what, String tagBase)
		{
			super(line, pos);
			this.type = what;
			this.tagBase = tagBase;
		}
		
		@Override
		public Type type()
		{
			return this.type;
		}

		@Override
		public String openTag()
		{
			return "<" + tagBase + ">";
		}

		@Override
		public String closeTag()
		{
			return "</" + this.tagBase + ">";
		}
		
	}
	
	private class BoldToken extends EmptyToken
	{

		public BoldToken(int line, int pos)
		{
			super(line, pos, Type.BOLD, "b");
		}

		@Override
		public String text()
		{
			return "*";
		}
		
	}
	
	private class ItalicToken extends EmptyToken
	{
		public ItalicToken(int line, int pos)
		{
			super(line, pos, Type.ITALIC, "i");
		}	

		@Override
		public String text()
		{
			return "/";
		}

	}
	
	private class UnderlineToken extends EmptyToken
	{
		public UnderlineToken(int line, int pos)
		{
			super(line, pos, Type.UNDERLINE, "u");
		}	
		@Override
		public String text()
		{
			return "_";
		}
		
	}
	
	private class LinkOpenToken extends EmptyToken
	{
		public LinkOpenToken(int line, int pos)
		{
			super(line, pos, Type.LINK_OPEN, "a");
		}	

		@Override
		public String closeTag()
		{
			return "\">";
		}

		@Override
		public String openTag()
		{
			return "<a href=\"";
		}

		@Override
		public String text()
		{
			return "[";
		}		
	}
	
	private class ImageOpenToken extends EmptyToken
	{

		public ImageOpenToken(int line, int pos)
		{
			super(line, pos, Type.IMAGE_OPEN, "img");
		}
		
		@Override
		public String closeTag()
		{
			return "\">";
		}

		@Override
		public String openTag()
		{
			return "<img src=\"";
		}

		@Override
		public String text()
		{
			return "{";
		}		
		
	}

	private class LinkCloseToken extends EmptyToken
	{
		public LinkCloseToken(int line, int pos)
		{
			super(line, pos, MarkdownToken.Type.LINK_CLOSE, "a");
		}		
		
		@Override
		public String text()
		{
			return "]";
		}
		
	}
	
	private class ImageCloseToken extends EmptyToken
	{

		public ImageCloseToken(int line, int pos)
		{
			super(line, pos, Type.IMAGE_CLOSE, "img");
		}

		@Override
		public String text()
		{
			return "}";
		}

		@Override
		public String closeTag()
		{
			return "\">";
		}

		@Override
		public String openTag()
		{
			return "";
		}
	}
	
	private class BulletToken extends EmptyToken
	{
		public BulletToken(int line, int pos)
		{
			super(line, pos, MarkdownToken.Type.BULLET, "");
		}	

		@Override
		public String closeTag()
		{
			return "</li><li>";
		}

		@Override
		public String openTag()
		{
			return "<ul><li>";
		}
		
		@Override
		public String text()
		{
			return "#";
		}
		
	}
	
	private class BlankToken extends EmptyToken
	{
		public BlankToken(int line, int pos)
		{
			super(line, pos, MarkdownToken.Type.BLANK, "");
		}	

		@Override
		public String openTag()
		{
			return "<br/>";
		}

		@Override
		public String closeTag()
		{
			return "</li></ul>";
		}
		
		@Override
		public String text()
		{
			return " ";
		}
		
	}

	private class TextToken extends PositionToken
	{
		private final String text;

		public TextToken(int line, int pos, String text)
		{
			super(line, pos);
			this.text = text;
		}

		@Override
		public Type type()
		{
			return Type.TEXT;
		}

		@Override
		public String text()
		{
			return this.text;
		}

		@Override
		public String openTag()
		{
			return "";
		}

		@Override
		public String closeTag()
		{
			return "";
		}
	}
	
	private class LwspToken extends TextToken
	{
		public LwspToken(int line, int pos, String text)
		{
			super(line, pos, " ");
		}

		@Override
		public Type type()
		{
			return Type.LWSP;
		}
	}
	
	private class ErrorToken extends PositionToken
	{
		private final String text;
		
		public ErrorToken(String text, int line, int pos)
		{
			super(line, pos);
			this.text = text;
		}

		@Override
		public Type type()
		{
			return Type.ERROR;
		}

		@Override
		public String text()
		{
			String header = "Unparsable text at line "+this.line()+", pos "+this.pos()+", starting:  ";
			if (this.text.length() > 20)
				header += this.text.substring(0, 20) + "...";
			else
				header += this.text;
			return header;
		}

		@Override
		public String openTag()
		{
			throw new UnsupportedOperationException(); 
		}

		@Override
		public String closeTag()
		{
			throw new UnsupportedOperationException(); 
		}
	}
}
