/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.phalanx.mdn;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Rene Lauer (ray@phalanx.cz)
 */
public class MarkdownLexerNGTest
{
	/**
	 * Test of tokenize method, of class MarkdownLexer.
	 */
	@Test
	public void testTokenizeBold()
	{
		System.out.println("tokenizeBold");
		MarkdownLexer instance = new MarkdownLexer();
		MarkdownToken[] result = instance.tokenize("*").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.BOLD, result[0].type());
		result = instance.tokenize("**").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.TEXT, result[0].type());
		assertEquals("*", result[0].text());
	}
	
	@Test
	public void testTokenizeItalic()
	{
		System.out.println("tokenizeItalic");
		MarkdownLexer instance = new MarkdownLexer();
		MarkdownToken[] result = instance.tokenize("/").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.ITALIC, result[0].type());
		result = instance.tokenize("//").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.TEXT, result[0].type());
		assertEquals("/", result[0].text());
	}

	@Test
	public void testTokenizeUnderline()
	{
		System.out.println("tokenizeUnderline");
		MarkdownLexer instance = new MarkdownLexer();
		MarkdownToken[] result = instance.tokenize("_").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.UNDERLINE, result[0].type());
		result = instance.tokenize("__").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.TEXT, result[0].type());
		assertEquals("_", result[0].text());
	}

	@Test
	public void testTokenizeLinkOpen()
	{
		System.out.println("tokenizeLinkOpen");
		MarkdownLexer instance = new MarkdownLexer();
		MarkdownToken[] result = instance.tokenize("[").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.LINK_OPEN, result[0].type());
		result = instance.tokenize("[[").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.TEXT, result[0].type());
		assertEquals("[", result[0].text());
	}

	@Test
	public void testTokenizeLinkClose()
	{
		System.out.println("tokenizeLinkClose");
		MarkdownLexer instance = new MarkdownLexer();
		MarkdownToken[] result = instance.tokenize("]").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.LINK_CLOSE, result[0].type());
		result = instance.tokenize("]]").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.TEXT, result[0].type());
		assertEquals("]", result[0].text());
	}

	@Test
	public void testTokenizeBullet()
	{
		System.out.println("tokenizeBullet");
		MarkdownLexer instance = new MarkdownLexer();
		MarkdownToken[] result = instance.tokenize("#").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.BULLET, result[0].type());
		result = instance.tokenize("##").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.TEXT, result[0].type());
		assertEquals("#", result[0].text());
	}

	@Test
	public void testTokenizeBlank()
	{
		System.out.println("tokenizeBlank");
		MarkdownLexer instance = new MarkdownLexer();
		MarkdownToken[] result = instance.tokenize("\n\n").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.BLANK, result[0].type());
		result = instance.tokenize("\ntext\n").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 3);
		assertEquals(MarkdownToken.Type.TEXT, result[0].type());
		assertEquals("\n", result[0].text());
		assertEquals(MarkdownToken.Type.TEXT, result[1].type());
		assertEquals("text", result[1].text());
		assertEquals(MarkdownToken.Type.TEXT, result[2].type());
		assertEquals("\n", result[2].text());
		result = instance.tokenize("\n\n\n").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 2);
		assertEquals(MarkdownToken.Type.BLANK, result[0].type());
		assertEquals(MarkdownToken.Type.TEXT, result[1].type());
		assertEquals("\n", result[1].text());

	}
	
	@Test
	public void testTokenizeMixed() throws Exception
	{
		System.out.println("tokenizeMixed");
		MarkdownLexer instance = new MarkdownLexer();
		MarkdownToken[] result = instance.tokenize("#*/_alfa_/* [beta beta]\n\n").toArray(MarkdownToken[]::new);	
		assertEquals(result.length, 15);
		MarkdownToken.Type[] expected = new MarkdownToken.Type[] {
			MarkdownToken.Type.BULLET, MarkdownToken.Type.BOLD, MarkdownToken.Type.ITALIC, MarkdownToken.Type.UNDERLINE,
			MarkdownToken.Type.TEXT,
			MarkdownToken.Type.UNDERLINE, MarkdownToken.Type.ITALIC, MarkdownToken.Type.BOLD,
			MarkdownToken.Type.LWSP,
			MarkdownToken.Type.LINK_OPEN,
			MarkdownToken.Type.TEXT,
			MarkdownToken.Type.LWSP,
			MarkdownToken.Type.TEXT,
			MarkdownToken.Type.LINK_CLOSE,
			MarkdownToken.Type.BLANK
		};
		for (int i = 0; i < 15; i ++)
			assertEquals(result[i].type(), expected[i]);
	}
	
	@Test
	public void testTokenizeImageOpen()
	{
		System.out.println("tokenizeImageOpen");
		MarkdownLexer instance = new MarkdownLexer();
		MarkdownToken[] result = instance.tokenize("{").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.IMAGE_OPEN, result[0].type());
		result = instance.tokenize("{{").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.TEXT, result[0].type());
		assertEquals("{", result[0].text());
	}

	@Test
	public void testTokenizeImageClose()
	{
		System.out.println("tokenizeImageClose");
		MarkdownLexer instance = new MarkdownLexer();
		MarkdownToken[] result = instance.tokenize("}").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.IMAGE_CLOSE, result[0].type());
		result = instance.tokenize("}}").toArray(MarkdownToken[]::new);
		assertEquals(result.length, 1);
		assertEquals(MarkdownToken.Type.TEXT, result[0].type());
		assertEquals("}", result[0].text());
	}
	
}
