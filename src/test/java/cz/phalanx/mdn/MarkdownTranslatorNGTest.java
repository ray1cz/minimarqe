/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.phalanx.mdn;

import static org.testng.Assert.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author Rene Lauer (ray@phalanx.cz)
 */
public class MarkdownTranslatorNGTest
{

	private MarkdownLexer LEXER;
	
	@BeforeClass
	public void setupClass()
	{
		LEXER = new MarkdownLexer();
	}
	
	@Test
	public void testXlatPlainText() throws Exception
	{
		System.out.println("xlatPlainText");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		String actual = instance.translate("\nsimple   \ttext\nwith\twhitespaces\n");
		String expected = "\nsimple text\nwith whitespaces\n";
		assertEquals(actual, expected);		
	}
	
	@Test
	public void testXlatBold() throws Exception
	{
		System.out.println("xlatBold");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		String actual = instance.translate("* this is bold ***");
		String expected = "<b> this is bold *</b>";
		assertEquals(actual, expected);
	}	

	@Test
	public void testXlatItalic() throws Exception
	{
		System.out.println("xlatItalic");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		String actual = instance.translate("/// this is italic/");
		String expected = "/<i> this is italic</i>";
		assertEquals(actual, expected);
	}	

	@Test
	public void testXlatUnderline() throws Exception
	{
		System.out.println("xlatUnderline");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		String actual = instance.translate("_underlined__text_");
		String expected = "<u>underlined_text</u>";
		assertEquals(actual, expected);
	}	

	@Test
	public void testXlatBreak() throws Exception
	{
		System.out.println("xlatBreak");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		String actual = instance.translate("i\n\nbroke\n\nthis\n\ntext");
		String expected = "i<br/>broke<br/>this<br/>text";
		assertEquals(actual, expected);
	}	

	@Test
	public void testXlatList() throws Exception
	{
		System.out.println("xlatList");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		String actual = instance.translate("#alpha\n#beta\n#gamma\n\n");
		String expected = "<ul><li>alpha\n</li><li>beta\n</li><li>gamma</li></ul>";
		assertEquals(actual, expected);
	}	
	
	@Test
	public void testXlatLink() throws Exception
	{
		System.out.println("xlatLink");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		String actual = instance.translate("[url desc]");
		String expected = "<a href=\"url\">desc</a>";
		assertEquals(actual, expected);
	}	
	
	@Test(expectedExceptions = MarkdownException.class)
	public void testMismatchedMarks() throws Exception
	{
		System.out.println("mismatchedMarks");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		instance.translate("*mismatch/");
	}

	@Test(expectedExceptions = MarkdownException.class)
	public void testUnclosedList() throws Exception
	{
		System.out.println("unclosedList");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		instance.translate("#alfa");
	}
	
	@Test(expectedExceptions = MarkdownException.class)
	public void testIncompleteLink() throws Exception
	{
		System.out.println("incompleteLink");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		String res = instance.translate("[]");
		assertEquals(res, "[]");
		instance.translate("[alfa]");
	}
	
	@Test
	public void testLinkMarkup() throws Exception
	{
		System.out.println("linkMarkup");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		String res = instance.translate("[*#_/# this should be ok]");
		assertEquals(res, "<a href=\"*#_/#\">this should be ok</a>");
	}
	
	@Test
	public void testXlatImage() throws Exception
	{
		System.out.println("xlatImage");
		MarkdownTranslator instance = new MarkdownTranslator(LEXER);
		String actual = instance.translate("{image_url}");
		String expected = "<img src=\"image_url\">";
		assertEquals(actual, expected);
	}	
	
}
